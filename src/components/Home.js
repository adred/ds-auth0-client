import React, { Fragment } from "react";
import Auth0Lock from "auth0-lock";

const Home = () => {
  const [profile, setProfile] = React.useState();

  const lock = React.useMemo(
    () =>
      new Auth0Lock(
        "gXnc2jSBAHb56pNwm6OroAGXHR5X2VAW",
        "harv-test.jp.auth0.com"
      ),
    []
  );

  const getUserInfo = React.useCallback((error, profileResult) => {
    if (error) {
      console.log(error);
      return;
    }

    const profile = profileResult;

    setProfile(profile);
  }, []);

  const checkSession = React.useCallback(
    (error, authResult) => {
      if (error) {
        console.log(error);
        return;
      }

      lock.getUserInfo(authResult.accessToken, getUserInfo);
    },
    [lock, getUserInfo]
  );

  const onAuthenticated = React.useCallback(
    (authResult) => {
      lock.getUserInfo(authResult.accessToken, getUserInfo);
    },
    [lock, getUserInfo]
  );

  const onSignIn = React.useCallback(() => {
    lock.show();
    lock.on("authenticated", onAuthenticated);
  }, [lock, onAuthenticated]);

  const onLogOut = React.useCallback(() => {
    lock.logout();
  }, [lock]);

  React.useEffect(() => {
    lock.checkSession({}, checkSession);
  }, [lock, checkSession]);

  return (
    <Fragment>
      <div className="container">
        <div className="jumbotron text-center mt-5">
          <h1>Auth0 + GoLang + ReactJS</h1>
          <p>An example of Auth0/GoLang/ReactJS Integration</p>
          {!profile ? (
            <button
              className="btn btn-primary btn-lg btn-login btn-block"
              onClick={() => onSignIn()}
            >
              Sign in
            </button>
          ) : (
            <button
              className="btn btn-primary btn-lg btn-login btn-block"
              onClick={() => onLogOut()}
            >
              Log out
            </button>
          )}
        </div>
      </div>
    </Fragment>
  );
};

export default Home;
