import React, { useState, useEffect } from "react";
import { useAuth0 } from "../react-auth0-spa";
import axios from "axios";
import SyntaxHighlighter from "react-syntax-highlighter";
import { docco } from "react-syntax-highlighter/dist/esm/styles/hljs";

const LoggedIn = () => {
  const [apiResponse, setApiResponse] = useState([]);

  const { getTokenSilently, loading, user, logout, isAuthenticated } =
    useAuth0();

  useEffect(() => {
    const getResponse = async () => {
      try {
        const token = await getTokenSilently();
        const response = await axios({
          url: "http://localhost:8080/graphql",
          method: "post",
          headers: {
            Authorization: `Bearer ${token}`,
          },
          data: {
            query: `
              {
                getAllUsers {
                  id
                  name
                  email
                  tasks {
                    id
                  }
                }
              }
            `,
          },
        });

        setApiResponse(response);
      } catch (error) {
        console.error(error);
      }
    };

    getResponse();
  }, [getTokenSilently]);

  if (loading || !user) {
    return <div>Loading...</div>;
  }

  console.log("apiResponse", apiResponse);

  return (
    <div className="container">
      <div className="jumbotron text-center mt-5">
        {isAuthenticated && (
          <span
            className="btn btn-primary float-right"
            onClick={() => logout()}
          >
            Log out
          </span>
        )}
        <h1>Auth0 + GoLang + ReactJS</h1>
        <p>Hi, {user.name}!</p>
        {apiResponse && (
          <pre>
            <SyntaxHighlighter language="json" style={docco}>
              {`${apiResponse?.data?.data?.getAllUsers}`}
            </SyntaxHighlighter>
          </pre>
        )}
      </div>
    </div>
  );
};

export default LoggedIn;
